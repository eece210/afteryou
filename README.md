# AfterYou #

This demonstrates the wait/notify pattern

Threads can add and remove elements from the same queue.  Completion is detected when all threads are waiting.