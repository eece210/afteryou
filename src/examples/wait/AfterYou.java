package examples.wait;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class AfterYou {

	private static class WaitManager {
		BlockingQueue<String> peopleQueue;
		
		Object waitLock;
		boolean waiting;
		boolean done;
		
		int numThreads;
		int numThreadsWaiting;
		
		public WaitManager(int numThreads) {
			this.numThreads = numThreads;
			numThreadsWaiting = 0;
			waiting = false;
			done = false;
			waitLock = new Object();
			peopleQueue = new LinkedBlockingQueue<String>();
		}
		
		public synchronized void printMessage(String msg) {
			System.out.println(msg);
		}
		
		/**
		 * @return next person, or null if queue is empty
		 */
		public String getNext() {
			return peopleQueue.poll();
		}
		
		/**
		 * Adds a person to the queue, if any threads are waiting, they are awoken
		 * @param person
		 */
		public void add(String person) {
			peopleQueue.add(person);
			if (waiting) {
				wakeup();
			}
		}
		
		/**
		 * Wake up any waiting threads
		 */
		private void wakeup() {
			synchronized (waitLock) {
				waitLock.notifyAll();
			}
			waiting = false;
		}
		
		/**
		 * Puts the current thread to sleep until the queue
		 * has more data (at which point all threads are notified)
		 */
		public void waitForContent() {
			
			synchronized (waitLock) {
				// check if all other threads are waiting
				if (numThreadsWaiting == (numThreads-1)) {
					// If the last threads is done with the queue, no more
					// content can be added
					done = true;
					wakeup();
				} else {
					waiting = true;
					numThreadsWaiting++;
					try {
						waitLock.wait();
					} catch (InterruptedException e) {
					}
					numThreadsWaiting--; // thread is awoken
				} // done checking if done
			} // done lock
		}
		
		/**
		 * @return true if queue is empty and all threads entered waiting state
		 *              (so nothing new will be added to the queue)
		 */
		public boolean isDone() {
			return done;
		}
		
	}
	
	private static class WaitWorker implements Runnable {
		WaitManager manager;
		Random random;
		double offerProbability;
		
		static String[] offerMessages = {"Please, after you!",
			"Go ahead, I'm in no rush.",
			"Sorry, after you.",
			"No, please, after you.",
			"Sorry, go ahead",
			"Go ahead, you arrived first.",
			"I insist, after you."
		};
		
		static String[] acceptMessages = {"Thank you!",
				"You're very kind.",
				"Thanks."};
		
		public WaitWorker(WaitManager manager, Random random, double offerProbability) {
			this.manager = manager;
			this.offerProbability = offerProbability;
			this.random = random;
		}

		@Override
		public void run() {
			
			while (!manager.isDone()) {
				
				String person = manager.getNext();
				
				if (person != null) {
					// check if we accept or offer
					if (random.nextDouble() < offerProbability) {
						// offer to next person in queue
						manager.printMessage(person +":\t " +offerMessages[random.nextInt(offerMessages.length-1)] );
						// sleep a bit to make this process slower
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {}
						manager.add(person);  // add person back to queue
					} else {
						manager.printMessage(person +":\t " +acceptMessages[random.nextInt(acceptMessages.length-1)] );
						manager.printMessage( "   <" + person + " leaves >" );
					}
				} else {
					// wait
					manager.printMessage( "   <" + Thread.currentThread().getName() + " is waiting >" );
					manager.waitForContent();
					manager.printMessage( "   <" + Thread.currentThread().getName() + " woke up >" );
				}
			}
		}
	}
	
	public static void main(String[] args) {
		
		int numThreads = 3;
		String[] people = {"John", "Jacob", "Steve", "Emily", "Eliza"};
		
		Thread[] threads = new Thread[numThreads];
		double offerProbability  = 0.9;  // 90% probability of letting someone else go first
		Random random = new Random(System.currentTimeMillis());
		WaitManager manager = new WaitManager(threads.length);
		
		System.out.println(people.length + " Canadians arrive at a door at the same time.  This is their conversation.");
		System.out.println();
		
		// add people to queue
		for (String person : people) {
			manager.add(person);
		}
		
		// set up threads
		for (int i=0; i<threads.length; i++) {
			threads[i] = new Thread(new WaitWorker(manager, random, offerProbability));
			threads[i].start();
		}
		
		// wait for completion
		for (int i=0; i<threads.length; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
			}
		}
		
		manager.printMessage("\n   < DONE!!!! >");
		
	}
	
}
